import React from 'react';
import {Link} from 'react-router';

import RaisedButton from 'material-ui/lib/raised-button';

class Home extends React.Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      <div>
        <h1>Home</h1>
        <Link to={'/somepage/42'} style={{marginRight:'20px'}}>
            <RaisedButton label="Go to some page (42)" primary={true} />
        </Link>
        <Link to={'/somepage/23'}>
            <RaisedButton label="Go to some page (23)" primary={true} />
        </Link>
      </div>
    )
  }

}

export default Home;