import React from 'react';
import {Router} from 'react-router';

import Header from '../layout/Header';

class Main extends React.Component {

    constructor(props) {
      super(props);
    }
    
    render() {
      return (
        <div>
          <Header title="React And Material UI Boilerplate" />
          <section style={{margin: '20px'}}>
            {this.props.children}
          </section>
        </div>
      )
    }
}

export default Main;