import React from 'react';
import {Router} from 'react-router';

import AppBar from 'material-ui/lib/app-bar';

class Header extends React.Component {

    constructor(props) {
        super(props);
    }
    
    render() {
        return (
          <AppBar
            title={this.props.title}
            iconClassNameRight="muidocs-icon-navigation-expand-more" />  
        );
    }
}

export default Header;