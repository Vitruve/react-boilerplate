import React from 'react';
import {Route, IndexRoute} from 'react-router';

import Main from './components/pages/Main';
import Home from './components/pages/Home';
import SomePage from './components/pages/SomePage';

export default (
    <Route path="/" component={Main}>
        <IndexRoute component={Home} />
        <Route path="somepage/:someparam" component={SomePage} />
    </Route>
);
